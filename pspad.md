# pspad

<details><summary> drop-down </summary>


<details><summary> link </summary>

https://pspad.poradna.net/release/pspad506_setup.exe        
https://pspad.poradna.net/release/pspad506_x64_setup.exe          

https://pspad.poradna.net/release/pspad506en.zip          
https://pspad.poradna.net/release/pspad506en_x64.zip             

https://pspad.poradna.net/devel/pspad507b735.zip       
https://pspad.poradna.net/devel/pspad507b735x64.zip        


</details>

<details><summary> text </summary>

```
https://pspad.poradna.net/release/pspad506_setup.exe
https://pspad.poradna.net/release/pspad506_x64_setup.exe
https://pspad.poradna.net/release/pspad506en.zip
https://pspad.poradna.net/release/pspad506en_x64.zip
https://pspad.poradna.net/devel/pspad507b735.zip
https://pspad.poradna.net/devel/pspad507b735x64.zip

```
</details>


</details>

```
https://pspad.poradna.net/release/pspad506_setup.exe
https://pspad.poradna.net/release/pspad506_x64_setup.exe
https://pspad.poradna.net/release/pspad506en.zip
https://pspad.poradna.net/release/pspad506en_x64.zip
https://pspad.poradna.net/devel/pspad507b735.zip
https://pspad.poradna.net/devel/pspad507b735x64.zip

```

## PSPad - current version 5.0.6 (589) 
> 8 April 2021
>> http://www.fosshub.com/PSPad.html      

***       

### setup
https://pspad.poradna.net/release/pspad506_setup.exe        
https://pspad.poradna.net/release/pspad506_x64_setup.exe          

### portable
https://pspad.poradna.net/release/pspad506en.zip          
https://pspad.poradna.net/release/pspad506en_x64.zip             




## PSPad unicode 5.0.7 (735) English
> 2022-09-16 03:56
>> https://www.fosshub.com/PSPad-devel.html      

***
per : https://forum.pspad.com/read.php?6,77625      
Download links, **32b version is suggested**:       
*PSPad 64 bit version doesn't contains scripting yet.*     

### portable

```
SHA1 hash:
8edd98bb7b6240493715dfdc0f1eda18bb68c957 pspad507b735.zip
210f5ca2f949525aaa61be3f70cb48f38135dbc4 pspad507b735x64.zip
```


This archive **contains modified files only**. The correct way how to get full functionality:         
1. Download and install latest full version first!       
2. Replace existing files with content of archive       



